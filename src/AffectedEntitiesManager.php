<?php

namespace Drupal\affected_by_promotion;

use Drupal\commerce_promotion\Entity\PromotionInterface;
use Drupal\commerce_promotion\Plugin\Commerce\PromotionOffer\PromotionOfferInterface;

/**
 * The AffectedEntitiesManager service.
 */
class AffectedEntitiesManager {

  /**
   * Get the query for finding the affected entities for a promotion.
   *
   * @param \Drupal\commerce_promotion\Entity\PromotionInterface $promotion
   *   The promotion to use for the query.
   * @param string $entity_type_id
   *   The entity type id. For example commerce_product.
   *
   * @return bool|\Drupal\Core\Database\Query\Query
   *   Return a query if the offer type supports it, FALSE otherwise.
   */
  public function getAffectedEntitiesQuery(PromotionInterface $promotion, $entity_type_id) {
    $offer = $promotion->getOffer();
    if (!$offer instanceof SupportsAffectedEntitiesQueryInterface) {
      return FALSE;
    }
    return $offer->getAffectedEntitiesQuery($entity_type_id);
  }

  /**
   * Get the query for finding the affected entities for an offer.
   *
   * @param \Drupal\commerce_promotion\Plugin\Commerce\PromotionOffer\PromotionOfferInterface $offer
   *   The offer to use for the query.
   * @param string $entity_type_id
   *   The entity type id. For example commerce_product.
   *
   * @return bool|\Drupal\Core\Database\Query\Query
   *   Return a query if the offer type supports it, FALSE otherwise.
   */
  public function getAffectedEntitiesQueryByOffer(PromotionOfferInterface $offer, $entity_type_id) {
    if (!$offer instanceof SupportsAffectedEntitiesQueryInterface) {
      return FALSE;
    }
    return $offer->getAffectedEntitiesQuery($entity_type_id);
  }

}
