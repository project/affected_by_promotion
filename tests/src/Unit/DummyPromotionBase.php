<?php

namespace Drupal\Tests\affected_by_promotion\Unit;

use Drupal\commerce_promotion\Entity\PromotionInterface;
use Drupal\commerce_promotion\Plugin\Commerce\PromotionOffer\PromotionOfferInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * The DummyPromotionBase class.
 */
abstract class DummyPromotionBase implements PromotionOfferInterface {

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    // @todo Implement getConfiguration() method.
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    // @todo Implement setConfiguration() method.
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    // @todo Implement defaultConfiguration() method.
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    // @todo Implement calculateDependencies() method.
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {
    // @todo Implement buildConfigurationForm() method.
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ) {
    // @todo Implement validateConfigurationForm() method.
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ) {
    // @todo Implement submitConfigurationForm() method.
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId() {
    // @todo Implement getPluginId() method.
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginDefinition() {
    // @todo Implement getPluginDefinition() method.
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeId() {
    // @todo Implement getEntityTypeId() method.
  }

  /**
   * {@inheritdoc}
   */
  public function apply(
    EntityInterface $entity,
    PromotionInterface $promotion
  ) {
    // @todo Implement apply() method.
  }

  /**
   * {@inheritdoc}
   */
  public function clear(EntityInterface $entity, PromotionInterface $promotion) {
    // @todo Implement clear() method.
  }

}
