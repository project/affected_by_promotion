<?php

namespace Drupal\Tests\affected_by_promotion\Unit;

use Drupal\affected_by_promotion\SupportsAffectedEntitiesQueryInterface;

/**
 * The DummyPromotionWithInterface class.
 */
class DummyPromotionWithInterface extends DummyPromotionBase implements SupportsAffectedEntitiesQueryInterface {

  /**
   * {@inheritdoc}
   */
  public function getAffectedEntitiesQuery($entity_type_id) {
    $query = \Drupal::database()->select($entity_type_id)->fields($entity_type_id);
    return $query;
  }

}
