<?php

namespace Drupal\Tests\affected_by_promotion\Unit;

use Drupal\affected_by_promotion\AffectedEntitiesManager;
use Drupal\commerce_promotion\Entity\PromotionInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Tests\UnitTestCase;

/**
 * Test tag name generation.
 *
 * @group affected_by_promotion
 */
class GetAffectedEntitiesTest extends UnitTestCase {

  /**
   * Test that we do not get a query when passing a promotion not supporting it.
   */
  public function testUnsupportedPromotion() {
    $manager = new AffectedEntitiesManager();
    $mock_promotion = $this->createMock(PromotionInterface::class);
    $mock_promotion->method('getOffer')
      ->willReturn(new DummyPromotionNoInterface());
    $this->assertEquals(FALSE, $manager->getAffectedEntitiesQuery($mock_promotion, 'commerce_product'));
  }

  /**
   * Test that we get a query when passing a promotion supporting it.
   */
  public function testsupportedPromotion() {
    $manager = new AffectedEntitiesManager();
    $mock_promotion = $this->createMock(PromotionInterface::class);
    $mock_promotion->method('getOffer')
      ->willReturn(new DummyPromotionWithInterface());
    // Inject the database service into the container.
    $container = new ContainerBuilder();
    $select = $this->createMock(SelectInterface::class);
    $select->method('fields')
      ->willReturn($select);
    $db = $this->createMock(Connection::class);
    $db->method('select')
      ->willReturn($select);
    $container->set('database', $db);
    \Drupal::setContainer($container);
    $this->assertNotEquals(FALSE, $manager->getAffectedEntitiesQuery($mock_promotion, 'commerce_product'));
  }

  /**
   * Test that we do not get a query when passing an offer not supporting it.
   */
  public function testUnsupportedOffer() {
    $manager = new AffectedEntitiesManager();
    $offer = new DummyPromotionNoInterface();
    $this->assertEquals(FALSE, $manager->getAffectedEntitiesQueryByOffer($offer, 'commerce_product'));
  }

  /**
   * Test that we get a query when passing an offer supporting it.
   */
  public function testsupportedOffer() {
    $manager = new AffectedEntitiesManager();
    $offer = new DummyPromotionWithInterface();
    // Inject the database service into the container.
    $container = new ContainerBuilder();
    $select = $this->createMock(SelectInterface::class);
    $select->method('fields')
      ->willReturn($select);
    $db = $this->createMock(Connection::class);
    $db->method('select')
      ->willReturn($select);
    $container->set('database', $db);
    \Drupal::setContainer($container);
    $this->assertNotEquals(FALSE, $manager->getAffectedEntitiesQueryByOffer($offer, 'commerce_product'));
  }

}
